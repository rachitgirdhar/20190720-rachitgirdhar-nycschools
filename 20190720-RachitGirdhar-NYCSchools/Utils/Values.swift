//
//  Values.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import Foundation


//Class to store all the constant values in the app
public class Values {
    public static let schoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    public static let satURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}
