//
//  Tools.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import UIKit

//Helper methods to reduce redundancy in the code.
public class Tools {
    
    //function to show alert with handler
    public static func alert(_ vc: UIViewController, title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        vc.present(alert, animated: true, completion: nil)
        
    }
    
    //function to show alert without handler
    public static func alert(_ vc: UIViewController, title: String, message: String){
        alert(vc, title: title, message: message, handler: nil)
    }
    
    
}
