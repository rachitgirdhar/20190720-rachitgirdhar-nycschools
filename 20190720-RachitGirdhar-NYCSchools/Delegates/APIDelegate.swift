//
//  APIDelegate.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import Foundation

//Protocol to handle responses from APICalls
public protocol APIDelegate: NSObjectProtocol {
    func didReceiveSuccessResponse(_ data: Data, requestID: Request.RC)
    func didReceiveFailResponse(_ requestID: Request.RC, _ error: Error?)
}
