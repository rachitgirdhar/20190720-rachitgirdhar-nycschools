//
//  APICalls.m
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

#import "APICalls.h"
#import "NYCSchools-Swift.h"


@implementation APICalls

- (void) getData: (NSString*) url requestID: (int) requestID callback: (void (^)(NSData *data, NSError *error))callback {
    
    NSURL* _url = [NSURL URLWithString: url];
    NSURLSession *session = NSURLSession.sharedSession;
    
    NSURLSessionDataTask *task = [session dataTaskWithURL:_url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(data, error);
        });
        
    }];
    
    [task resume];
    
//    let url = getURL(keyword: keyword)
//    let furl:String = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
//    let session = URLSession.shared
//
//    Log.wtf(TAG, furl)
//    Log.wtf(TAG, getAuthToken())
//
//    if(!isConnected()){
//        return false
//    }
//
//    var request = URLRequest(url: URL(string: url)!)
//    request.addValue(getAuthToken(), forHTTPHeaderField: "Authorization")
//
//
//    session.dataTask(with: request) { (data, response, error) in
//        var code = 300
//        if let res = response as? HTTPURLResponse{
//            code = res.statusCode
//            if(code == 400){
//                self.performSelector(onMainThread: #selector(ApiCalls.sendError(_:)), with: [requestID, code, error], waitUntilDone: false)
//                return
//            }else if(code > 299){
//                self.performSelector(inBackground: #selector(ApiCalls.sendError(_:)), with: [requestID, code, error])
//                return
//            }
//        }else{
//            return
//        }
//        if(error != nil){
//            self.performSelector(onMainThread: #selector(ApiCalls.sendError(_:)), with: [requestID, code, error], waitUntilDone: false)
//            Log.wtf(self.TAG, "Error \(requestID) : \(error)")
//            return
//        }
//        self.performSelector(onMainThread: #selector(ApiCalls.sendBack(_:)), with: [data, requestID], waitUntilDone: false)
//
//    }.resume()
}

@end
