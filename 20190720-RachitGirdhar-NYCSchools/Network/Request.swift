//
//  Request.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import Foundation

//Class to make API requests
public class Request {
    
    
    
    var apiCalls: APICalls!
    var apiDelegate: APIDelegate!
    
    public init(_ apiDelegate: APIDelegate){
        apiCalls = APICalls()
        self.apiDelegate = apiDelegate
    }
    
    public enum RC: Int {
        case SCHOOLS, SAT
    }
    
    
    //GET REQUESTS
    public func getSchools(_ limit: Int, _ offSet: Int) {
        let url = "\(Values.schoolURL)?$limit=\(limit)&$offset=\(offSet)"
        get(url, requestID: .SCHOOLS)
    }
    
    public func getSATScors(_ dbn: String){
        let url = "\(Values.satURL)?dbn=\(dbn)"
        get(url, requestID: .SAT)
    }
    
    
    //PRIVATE METHODS
    private func get(_ url: String, requestID: Request.RC) {
        apiCalls.getData(url, requestID: Int32(requestID.rawValue)) { (data, err) in
            if(data == nil){
                self.apiDelegate.didReceiveFailResponse(requestID, err)
            }else{
                self.apiDelegate.didReceiveSuccessResponse(data!, requestID: requestID)
            }
        }
    }
    
}
