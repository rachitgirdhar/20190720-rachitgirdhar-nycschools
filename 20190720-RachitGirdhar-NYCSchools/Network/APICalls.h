//
//  APICalls.h
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef APICalls_h
#define APICalls_h


#endif /* APICalls_h */

@interface APICalls : NSObject

- (void) getData: (NSString*) url requestID: (int) requestID callback: (void (^)(NSData *data, NSError *error))callback;

@end
