//
//  BaseVC.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import UIKit


//Base View Contoller which all view controllers extend. This is to reduce reduncancy in the code.
class BaseVC: UIViewController, APIDelegate {
    
    
    
    var request: Request!
    var decoder: JSONDecoder!
    
    var loadingView: UIView!
    var container: UIView!
    var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        request = Request(self)
        decoder = JSONDecoder()
        
        loadingView = UIView()
        container = UIView()
        activityIndicator = UIActivityIndicatorView()

    }
    
    func didReceiveSuccessResponse(_ data: Data, requestID: Request.RC) {
        
    }
    
    func didReceiveFailResponse(_ requestID: Request.RC, _ error: Error?) {
        NSLog("Call Failed: \(requestID) Error:\(error)")
    }

    
    public func showLoader() {
        
        let win:UIWindow = UIApplication.shared.delegate!.window!!
        self.loadingView = UIView(frame: win.frame)
        self.loadingView.tag = 1000
        self.loadingView.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0)
        
        win.addSubview(self.loadingView)
        
        container = UIView(frame: CGRect(x: 0, y: 0, width: win.frame.width/3, height: win.frame.width/3))
        container.backgroundColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.6)
        container.layer.cornerRadius = 10.0
        container.layer.borderColor = UIColor.gray.cgColor
        container.layer.borderWidth = 0.5
        container.clipsToBounds = true
        container.center = self.loadingView.center
        
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: win.frame.width/5, height: win.frame.width/5)
        activityIndicator.style = .whiteLarge
        activityIndicator.center = self.loadingView.center
        
        
        self.loadingView.addSubview(container)
        self.loadingView.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
    }
    
    public func hideLoader(){
        UIView.animate(withDuration: 0.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.container.alpha = 0.0
            self.loadingView.alpha = 0.0
            self.activityIndicator.stopAnimating()
        }, completion: { finished in
            self.activityIndicator.removeFromSuperview()
            self.container.removeFromSuperview()
            self.loadingView.removeFromSuperview()
            let win:UIWindow = UIApplication.shared.delegate!.window!!
            let removeView  = win.viewWithTag(1000)
            removeView?.removeFromSuperview()
        })
    }
}
