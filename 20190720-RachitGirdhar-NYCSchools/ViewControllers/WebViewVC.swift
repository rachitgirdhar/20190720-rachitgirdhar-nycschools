//
//  WebViewVC.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: BaseVC, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    var url: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(!url.starts(with: "https")){
            url = "https://\(url!)"
        }
        
        guard let  _url = URL(string: url) else {
            showError()
            return
        }
        showLoader()
        webView.load(URLRequest(url: _url))
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        hideLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        showError()
    }
    
    private func showError(){
        hideLoader()
        Tools.alert(self, title: "Error Loading Website", message: "Sorry, there seems to be an issue white loading this website. Please try again.") { (action) in
            self.dismiss(animated: true, completion: nil)
        }
    }
}
