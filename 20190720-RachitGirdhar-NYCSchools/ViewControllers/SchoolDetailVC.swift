//
//  SchoolDetailVC.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import UIKit
import MessageUI

class SchoolDetailVC: BaseVC, MFMailComposeViewControllerDelegate {
    
    //Views
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var scoreView: UIView!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var schoolOverviewTextView: UITextView!
    @IBOutlet weak var addressLineOne: UILabel!
    @IBOutlet weak var addressLineTwoLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var websiteButton: UIButton!
    
    //Models
    var school: School!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        schoolNameLabel.text = school.school_name
        schoolOverviewTextView.text = school.overview_paragraph
        addressLineOne.text = school.primary_address_line_1
        addressLineTwoLabel.text = "\(school.city!), \(school.state_code!) \(school.zip!)"
        
        phoneNumberButton.setTitle(school.phone_number, for: .normal)
        emailButton.setTitle(school.school_email, for: .normal)
        websiteButton.setTitle(school.website, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        request.getSATScors(school.dbn)
        showLoader()
    }
    
    
    override func didReceiveSuccessResponse(_ data: Data, requestID: Request.RC) {
        switch requestID {
        case .SAT:
            showSats(data)
        default:
            super.didReceiveSuccessResponse(data, requestID: requestID)
        }
    }
    
    private func showSats(_ data: Data){
        hideLoader()
        var scores : [SATScore]!
        do {
            scores = try decoder.decode([SATScore].self, from: data)
        }catch{
            scoreView.isHidden = true
            NSLog("Error Parsing SAT Score")
            return
        }
        
        if(scores == nil || scores.count == 0){
            scoreView.isHidden = true
            return
        }
        
        scoreView.isHidden = false
        school.satScore = scores[0]
        mathScoreLabel.text = school.satScore.sat_math_avg_score
        readingScoreLabel.text = school.satScore.sat_critical_reading_avg_score
        writingScoreLabel.text = school.satScore.sat_writing_avg_score
        
    }

    
    @IBAction func phoneNumberTapped(_ sender: Any) {
        if let url = URL(string: "tel://\(school.phone_number!)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func emailTapped(_ sender: Any) {
        
        if(!MFMailComposeViewController.canSendMail()){
            Tools.alert(self, title: "Email Error", message: "Sorry, your phone doesn't support sending emails.")
            return
        }
        
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([school.school_email])
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func websiteTapped(_ sender: Any) {
        
        self.performSegue(withIdentifier: "detail_to_web", sender: self)
        
    }
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: self)
        
        if let webView = segue.destination as? WebViewVC {
            webView.url = school.website
        }
    }
}
