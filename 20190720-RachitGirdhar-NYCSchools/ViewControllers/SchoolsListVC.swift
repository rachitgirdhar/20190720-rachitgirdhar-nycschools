//
//  SchoolsListVC.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import UIKit

class SchoolsListVC: BaseVC, UITableViewDelegate, UITableViewDataSource {
    
    //Views
    @IBOutlet weak var tableView: UITableView!
    
    //Models
    var schools: [School]!
    
    //Constants
    private let limit = 20
    
    //Variables
    private var offSet = 0
    private var selectedSchool: School!

    override func viewDidLoad() {
        super.viewDidLoad()

        schools = [School]()
        tableView.delegate = self
        tableView.dataSource = self
        
        getSchools()
    }
    
    
    private func getSchools(){
        request.getSchools(limit, offSet)
        offSet += limit
        showLoader()
    }
    

    override func didReceiveSuccessResponse(_ data: Data, requestID: Request.RC) {
        switch requestID {
        case .SCHOOLS:
            showSchools(data)
        default:
            super.didReceiveSuccessResponse(data, requestID: requestID)
        }
    }
    
    private func showSchools(_ data: Data){
        hideLoader()
        var newSchools : [School]!
        do {
            newSchools = try decoder.decode([School].self, from: data)
        } catch {
            NSLog("Error Parsing School Data")
        }
        if(newSchools != nil){
            schools.append(contentsOf: newSchools)
            tableView.reloadData()
        }
    }
    
    
    //Table view methods begin
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "school_cell", for: indexPath)
        let school = schools[indexPath.row]
        let name = cell.viewWithTag(1) as! UILabel
        name.text = "\(indexPath.row + 1). \(school.school_name!)"
        let add = cell.viewWithTag(2) as! UILabel
        add.text = school.primary_address_line_1
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == schools.count - 1 {
            getSchools()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedSchool = schools[indexPath.row]
        self.performSegue(withIdentifier: "school_to_detail", sender: self)
    }
    //TableView Methods end
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let detail = segue.destination as? SchoolDetailVC {
            detail.school = selectedSchool
        }
    }

}
