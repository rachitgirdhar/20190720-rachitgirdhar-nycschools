//
//  School.swift
//  20190720-RachitGirdhar-NYCSchools
//
//  Created by Rachit Girdhar on 20/07/19.
//  Copyright © 2019 Daemon. All rights reserved.
//

import Foundation

public class School: Codable {
    var dbn, school_name, overview_paragraph, location, phone_number, school_email, website, city, zip, state_code, primary_address_line_1: String!
    var satScore: SATScore!
}
